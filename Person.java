package com.oneToOne.module;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "person1")
public class Person {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int id;
	@Column
	String name;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private AdharNo adharNo;

	public Person() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AdharNo getAdharNo() {
		return adharNo;
	}

	public void setAdharNo(AdharNo adharNo) {
		this.adharNo = adharNo;
	}

}
