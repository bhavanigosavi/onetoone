package com.oneToOne.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.oneToOne.module.Person;
import com.oneToOne.service.PersonService;

@RestController
public class PersonController {

	@Autowired
	private PersonService personService;

	@PostMapping(value = "/persons", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Person> saveAllPerson(@RequestBody Person person) {
		Person person1 = personService.savePerson(person);
		return ResponseEntity.status(201).body(person1);
	}

	@GetMapping(value = "/persons", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<Person>> getAllPerson1Details() {
		List<Person> person1 = personService.getAllPerson();
		return ResponseEntity.status(201).body(person1);

	}

	@PutMapping(value = "/persons/{id}")
	public ResponseEntity<Person> updatePerson(@PathVariable("id") Integer id, @RequestBody Person person) {
		Person person1 = personService.updatePerson(id, person);
		return ResponseEntity.status(200).body(person1);
	}
}
