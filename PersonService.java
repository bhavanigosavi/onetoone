package com.oneToOne.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oneToOne.module.Person;
import com.oneToOne.repository.PersonRepository;

@Service
public class PersonService {

	@Autowired
	private PersonRepository personRepository;

	public Person savePerson(Person person) {
		return personRepository.save(person);

	}

	public List<Person> getAllPerson() {
		return personRepository.findAll();

	}

	public Person updatePerson(Integer id, Person person) {
		person.setId(id);
		return personRepository.saveAndFlush(person);
	}
}
