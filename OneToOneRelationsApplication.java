package com.oneToOne.module;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com")
@EnableJpaRepositories(basePackages = "com.oneToOne.repository")
public class OneToOneRelationsApplication {

	public static void main(String[] args) {
		SpringApplication.run(OneToOneRelationsApplication.class, args);
	}

}
